# ngrok-tool

It's a docker version ngrok image create by Dmitry Shkoliar(https://github.com/shkoliar).  
Thank him for this tool.   
I just copy it into gitlab and build image here

## Usage
In k8s folder, there is a deployment that we can use ngrok to expose service inside cluster for debugging.  
Just change the env for pod setting
```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    k8s-app: ngrok-tool
  name: ngrok-tool
spec:
  selector:
    matchLabels:
      k8s-app: ngrok-tool
  template:
    metadata:
      annotations:
      labels:
        k8s-app: ngrok-tool
    spec:
      containers:
      - image: registry.gitlab.com/pagi-tools/ngrok-tool:v1.0.0
        imagePullPolicy: IfNotPresent
        name: ngrok-tool
        env:
        - name: DOMAIN
          value: "10.100.119.35"
        - name: PORT
          value: "8888"
        - name: PROTOCOL
          value: "tcp"
        - name: DEBUG
          value: "1"
        - name: AUTH_TOKEN
          value: ""


```

## Environment variables
Following is each parms setting or check README.org.md.  
List of available environment variables to configure ngrok in command line usage or as part of docker-compose.yml file.

| Name        | Values                     | Default   | Information                                                                                                                                                 |
| :---------- | :------------------------- | :-------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| PROTOCOL    | http, tls, tcp             | http      | Ngrok tunneling protocol.                                                                                                                                   |
| DOMAIN      | \*                         | localhost | Hostname or docker container, service name which is referred to by ngrok.                                                                                   |
| PORT        | \*                         | 80        | Port which is referred to by ngrok.                                                                                                                         |
| REGION      | us, eu, ap, au, sa, jp, in | us        | Region where the ngrok client will connect to host its tunnels.                                                                                             |
| HOST_HEADER | \*                         |           | Optional, rewrite incoming HTTP requests with a modified Host header. e.g. `HOST_HEADER=localdev.test`                                                      |
| BIND_TLS    | true, false                |           | Optional, forward only HTTP or HTTPS traffic, but not both. By default, when ngrok runs an HTTP tunnel, it opens endpoints for both HTTP and HTTPS traffic. |
| SUBDOMAIN   | \*                         |           | Optional, specifies the subdomain to use with ngrok, if unspecified ngrok with generate a unique subdomain on each start.                                   |
| AUTH_TOKEN  | \*                         |           | Optional, token used to authorise your subdomain with ngrok.                                                                                                 |
| DEBUG       | true                       |           | Optional, write logs to stdout.                                                                                                                             |
| PARAMS      | \*                         |           | Pass all ngrok parameters by one string. When specified, any other env variables are skipped (Except AUTH_TOKEN).|

For more information about ngrok parameters, please refer to [ngrok documentation](https://ngrok.com/docs).

